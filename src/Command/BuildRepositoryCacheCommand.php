<?php
/**
 * Synga Inheritance Finder
 * @author      Roy Pouls
 * @copytright  2016 Roy Pouls / Synga (http://www.synga.nl)
 * @license     http://www.opensource.org/licenses/mit-license.php MIT
 * @link        https://github.com/synga-nl/inheritance-finder
 */

namespace Synga\ModelManager\Command;


use Illuminate\Console\Command;
use Synga\ModelManager\Finder\Model;
use Synga\ModelManager\Finder\Repository;

class BuildRepositoryCacheCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'model-manager:cache';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Gets all phpstorm-meta classes and gives you the option to exclude them';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Model $modelFinder, Repository $repositoryFinder) {
        $models       = $modelFinder->find();
        $repositories = $repositoryFinder->find();


        $models = [
            'models'       => $models['combined'],
            'repositories' => $repositories['combined']
        ];

        $this->createDirectory();

        file_put_contents(storage_path('model-manager') . '/models.cache', serialize($models));
    }

    protected function createDirectory() {
        if (!file_exists(storage_path('model-manager'))) {
            mkdir(storage_path('model-manager'));
        }
    }
}