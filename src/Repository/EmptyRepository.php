<?php
/**
 * Synga Inheritance Finder
 * @author      Roy Pouls
 * @copytright  2016 Roy Pouls / Synga (http://www.synga.nl)
 * @license     http://www.opensource.org/licenses/mit-license.php MIT
 * @link        https://github.com/synga-nl/inheritance-finder
 */

namespace Synga\ModelManager\Repository;


use Illuminate\Contracts\Container\Container;
use Prettus\Repository\Eloquent\BaseRepository;

class EmptyRepository extends BaseRepository
{
    protected $emptyModel;

    public function __construct(Container $app, $model = null) {
        $this->prepareModel($model);
        parent::__construct($app);
    }

    public function model() {
        return $this->emptyModel;
    }

    protected function prepareModel($model){
        if(!empty($model)){
            if (is_string($model)) {
                $model = ltrim($model);
            }

            $this->emptyModel = $model;
        }
    }
}