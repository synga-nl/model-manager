<?php
/**
 * Synga Inheritance Finder
 * @author      Roy Pouls
 * @copytright  2016 Roy Pouls / Synga (http://www.synga.nl)
 * @license     http://www.opensource.org/licenses/mit-license.php MIT
 * @link        https://github.com/synga-nl/inheritance-finder
 */

namespace Synga\ModelManager\Finder;


use Synga\InheritanceFinder\InheritanceFinderInterface;

abstract class FinderAbstract
{
    /**
     * @var InheritanceFinderInterface
     */
    protected $inheritanceFinder;

    public function __construct(InheritanceFinderInterface $inheritanceFinder) {
        $this->inheritanceFinder = $inheritanceFinder;
    }

    abstract public function find();

    /**
     * @param $duplicates
     * @param $name
     * @param $class
     * @param $addedClass
     */
    protected function manageDuplicates(&$duplicates, $name, $class, $addedClass) {
        if (!isset($duplicates[$name])) {
            $duplicates[$name] = [$class, $addedClass];
        } else {
            $duplicates[$name][] = $class;
        }
    }

    /**
     * @param $content
     * @param $duplicates
     * @return mixed
     */
    protected function combineDuplicates($content, $duplicates) {
        $iterator = new \RecursiveIteratorIterator(new \RecursiveArrayIterator($duplicates));
        foreach ($iterator as $class) {
            if (!in_array($class, $content)) {
                $content[$class] = $class;
            }
        }

        return $content;
    }
}