<?php
/**
 * Synga Inheritance Finder
 * @author      Roy Pouls
 * @copytright  2016 Roy Pouls / Synga (http://www.synga.nl)
 * @license     http://www.opensource.org/licenses/mit-license.php MIT
 * @link        https://github.com/synga-nl/inheritance-finder
 */

namespace Synga\ModelManager\Finder;


class Model extends FinderAbstract
{
    public function find() {
        $duplicates = [];
        $content    = [];

        $models = $this->inheritanceFinder->findExtends('Illuminate\Database\Eloquent\Model');

        foreach ($models as $model) {
            $class                  = $model->getClass();
            $fullQualifiedNamespace = $model->getFullQualifiedNamespace();

            if (isset($content[$model->getClass()])) {
                $this->manageDuplicates($duplicates, $class, $fullQualifiedNamespace, $content[$class]);

                if ($this->inheritanceFinder->isSubclassOf($fullQualifiedNamespace, $content[$class])) {
                    $content[$class] = $fullQualifiedNamespace;
                }
            } else {
                $content[$class] = $fullQualifiedNamespace;
            }
        }

        return [
            'content' => $content,
            'duplicates' => $duplicates,
            'combined' => $this->combineDuplicates($content, $duplicates)
        ];
    }
}