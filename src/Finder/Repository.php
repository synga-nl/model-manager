<?php
/**
 * Synga Inheritance Finder
 * @author      Roy Pouls
 * @copytright  2016 Roy Pouls / Synga (http://www.synga.nl)
 * @license     http://www.opensource.org/licenses/mit-license.php MIT
 * @link        https://github.com/synga-nl/inheritance-finder
 */

namespace Synga\ModelManager\Finder;


use Synga\InheritanceFinder\InheritanceFinderInterface;
use Synga\InheritanceFinder\PhpClass;
use Synga\PhpStormMeta\Laravel\Resolver;

/**
 * Class Repository
 * @package Synga\ModelManager\Finder
 */
class Repository extends FinderAbstract
{
    /**
     * @var Model
     */
    private $modelFinder;
    /**
     * @var Resolver
     */
    private $resolver;

    /**
     * Repository constructor.
     * @param InheritanceFinderInterface $inheritanceFinder
     * @param Model $modelFinder
     * @param Resolver $resolver
     */
    public function __construct(InheritanceFinderInterface $inheritanceFinder, Model $modelFinder, Resolver $resolver) {
        parent::__construct($inheritanceFinder);
        $this->modelFinder = $modelFinder;
        $this->resolver    = $resolver;
    }

    /**
     *
     */
    public function find() {
        $content    = [];
        $duplicates = [];

        $repositories = $this->inheritanceFinder->findExtends('\Prettus\Repository\Eloquent\BaseRepository');

        $models = $this->modelFinder->find();

        foreach ($repositories as $repository) {
            $fullQualifiedNamespace = $repository->getFullQualifiedNamespace();


            if ($fullQualifiedNamespace !== 'Synga\ModelManager\Repository\EmptyRepository') {
                try {
                    if ($repository->getClassType() === PhpClass::TYPE_CLASS || $repository->getClassType() === PhpClass::TYPE_FINAL_CLASS) {
                        $repositoryObject = $this->resolver->resolve($repository->getFullQualifiedNamespace());
                        $modelName        = $this->getClassName($repositoryObject->model());

                        if (in_array($modelName, $models['content'])) {
                            if (isset($content[$modelName])) {
                                $this->manageDuplicates($duplicates, $modelName, $fullQualifiedNamespace, $content[$content[$modelName]]);
                            } else {
                                $content[array_search($modelName, $models['content'])] = $repository->getFullQualifiedNamespace();
                            }
                        }
                    }
                } catch (\Exception $e) {
//                    var_dump($e->getMessage());
                }
            }
        };

        $combinedContent = $content;

        foreach($models['combined'] as $modelName => $modelCheck){
            if(!isset($combinedContent[$modelCheck]) && !isset($combinedContent[$modelName])){
                $combinedContent[$modelName] = '\Synga\ModelManager\Repository\EmptyRepository';
            }
        }

        return [
            'content'    => $content,
            'duplicates' => $duplicates,
            'combined'   => $combinedContent
        ];
    }

    /**
     * @param $object
     * @return string
     */
    protected function getClassName($object) {
        if (is_object($object)) {
            return get_class($object);
        }

        return $object;
    }
}