<?php
/**
 * Synga Inheritance Finder
 * @author      Roy Pouls
 * @copytright  2016 Roy Pouls / Synga (http://www.synga.nl)
 * @license     http://www.opensource.org/licenses/mit-license.php MIT
 * @link        https://github.com/synga-nl/inheritance-finder
 */

namespace Synga\ModelManager;


use Synga\ModelManager\Finder\Model;
use Synga\ModelManager\Finder\Repository;
use Synga\PhpStormMeta\BuilderFactory;
use Synga\PhpStormMeta\PhpStormMetaExtensionInterface;

class PhpstormMeta implements PhpStormMetaExtensionInterface
{
    /**
     * @var Model
     */
    private $modelFinder;

    /**
     * @var Repository
     */
    private $repositoryFinder;

    public function __construct(Model $modelFinder, Repository $repositoryFinder) {
        $this->modelFinder      = $modelFinder;
        $this->repositoryFinder = $repositoryFinder;
    }

    public function execute(BuilderFactory $factory) {
        $models       = $this->modelFinder->find();
        $repositories = $this->repositoryFinder->find();

        $factory->addClassMethod()
            ->setClass('Synga\ModelManager\ModelManagerInterface')
            ->setMethod('getModel')
            ->setAllContent($models['combined']);

        $factory->addClassMethod()
            ->setClass('Synga\ModelManager\ModelManagerInterface')
            ->setMethod('getRepository')
            ->setAllContent($repositories['combined']);

        \Artisan::call('model-manager:cache');
    }
}