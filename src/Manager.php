<?php
/**
 * Synga Inheritance Finder
 * @author      Roy Pouls
 * @copytright  2016 Roy Pouls / Synga (http://www.synga.nl)
 * @license     http://www.opensource.org/licenses/mit-license.php MIT
 * @link        https://github.com/synga-nl/inheritance-finder
 */

namespace Synga\ModelManager;


use Synga\ModelManager\Repository\EmptyRepository;
use Illuminate\Container\Container;

class Manager implements ModelManagerInterface
{
    /**
     * @var array
     */
    protected $models;

    /**
     * @var array
     */
    protected $repositories;

    /**
     * @var array
     */
    protected $repositoryCache = [];
    /**
     * @var Container
     */
    private $container;

    public function __construct(Container $container) {
        $this->container = $container;
        $cacheData       = @unserialize(file_get_contents(storage_path('model-manager') . '/models.cache'));

        if (isset($cacheData['models'])) {
            $this->models = $cacheData['models'];
        }

        if (isset($cacheData['repositories'])) {
            $this->repositories = $cacheData['repositories'];
        }
    }


    public function getRepository($modelName) {
        if (!empty($this->repositoryCache[$modelName])) {
            return $this->repositoryCache[$modelName];
        }

        if (!empty($this->models[$modelName])) {
            if (!empty($this->repositories[$modelName])) {
                $class = ltrim($this->repositories[$modelName], '\\');

                return new $class($this->container, $modelName);
            } else {
                $repository = new EmptyRepository($this->container, $modelName);
                $repository->setModel($modelName);
            }
        }
    }

    public function getModel($modelName) {
        if (!empty($this->models[$modelName])) {
            $class = $this->models[$modelName];

            return new $class();
        }

        return false;
    }
}